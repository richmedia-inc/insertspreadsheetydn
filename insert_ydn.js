/**
 * YDNのScrapingのアカウント
 */
var fs = require('fs');
var casper = require('casper').create({
  pageSettings : {
    webSecurityEnabled : false
  },
});

// タイムアウト秒数
var TIMEOUT_IN_MS = 120000;

var ydnLoginUrl = 'https://login.bizmanager.yahoo.co.jp/login';
var ydnAdPage = 'https://yadui.business.yahoo.co.jp/biz/adv/Campaigns/index?io_id=';
var ydnUserId = casper.cli.has('user') ? casper.cli.get('user') : null;
var ydnPassword = casper.cli.has('pass') ? casper.cli.get('pass') : null;
var period = casper.cli.has('period') ? casper.cli.get('period') : null;
var accountIds = [1001486474, 1001618497, 1001602044, 1001703482, 1001560106, 1001633415]; // 現状使われている取得対象のアカウントIDのリスト
var crumb = ''; // YDN内の/api/配下のURLにアクセスするときに使うSessionIDに相当と思われる文字列
var time = new Date().getTime(); 

// 引数の解析
if (ydnUserId === null || ydnPassword === null) {
  casper.echo('引数が不足しています。 --user=username --pass=password');
  casper.exit(-1);
}

// ユーザーエージェント
var ua = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.74.9 (KHTML, like Gecko) Version/7.0.2 Safari/537.74.9";
casper.userAgent(ua);

casper.start();
//
// ログイン処理
//
casper.thenOpen(ydnLoginUrl, function() {
  // なぜか 2 回目アクセス時までログインフォーム内の
  // .crumb という値が決まらないのでこれはダミーのアクセス
  this.wait(1000);
});
casper.thenOpen(ydnLoginUrl, function() {
  // 待ち時間を挟まないとformの入力に失敗する
  this.wait(3000);
});
casper.then(function() {
  this.fill('form[name="login_form"]', {
    user_name : ydnUserId,
    password : ydnPassword
  }, true);
});
//
// 後続処理で使う SessionID 的な値を取得する
//
casper.thenOpen('https://promotionalads.business.yahoo.co.jp/Agency/UserProfile', function(response) {
  this.wait(5000);
});
casper.then(function() {
  crumb = this.evaluate(function() {
    // memo: YDNのページはjQueryを読み込んでいる
    return $('input[name="crumb"]').val();
  });
});

//csvを出力
casper.then(function() {
  var page = 1;
  var total = 0;

  casper.each(accountIds, function(self, accountId) {
    // Ajax で取ってきているデータに直接アクセスする
    this.thenOpen('https://promotionalads.business.yahoo.co.jp/Agency/Ajax/ContentsAds/Reports/Report/Find.php?_=' + time + '&io_id='+ accountId +'&crumb=' + crumb + '&results=&targetType=tsukiji', function() {
      this.wait(3000);
    });
    casper.then(function() {
      // 下記の条件を満たすダウンロード用の URL の構成要素を取ってくる
      var name = '';
      var job_id = '';

      // それぞれのアカウントの対象期間レポートを取得してくる。
      if (period == 'monthly') {
        targetSpan = 'Monthly';
      } else if(period == 'daily') {
        targetSpan = "Daily"
      }
      var reg_PerformanceReport = new RegExp('^' + targetSpan + '_\\s*パフォーマンスレポート');
      var ads = JSON.parse(this.getPageContent());
      self.each(ads.ResultSet.Result, function(self2, elem) {
        var data = elem.Data;
        var job_finished_on = data.jobFinishedOn;
        var job_status = data.jobStatus;
        // && job_finished_on.indexOf(targetDate) >= 0
        if (job_id === '' && job_status === 'COMPLETED') {
          var reportName = data.reportName.trim();
          if (reportName.match(reg_PerformanceReport) !== null) {
            // each 外からも参照できる変数に保存用の値を退避
            name = reportName;
            job_id = data.jobId; //定期レポートごとのユニークなIDが振られる
            var ad_page = function() {
              //レポートごとにデータを取得する。。
              var url = 'https://promotionalads.business.yahoo.co.jp/Agency/Ajax/ContentsAds/Reports/ReportPreview/Find.php?_='+ time +'&io_id='+ accountId +'&reportJobId='+ job_id +'&crumb='+ crumb +'&results=200&page=' + page;
              casper.thenOpen(url, function() {
                //画面を開いてから待機
                this.wait(5000);
              });
              this.then(function(){
                //ここに処理をかく
                var content = JSON.parse(this.getPageContent());
                total = content.ResultSet.Info.total;
                results = content.ResultSet.Result;

                casper.each(results, function(casper, result) {
                  var field_td = [];
                  //取得してきたデータに余計な文字列が含まれるので省く。
                  var AD_NAME = result.Data.AD_NAME.replace(/&quot;/g,"");
                  var URL_NAME = result.Data.URL_NAME.replace(/&quot;/g,"");
                  var CAMPAIGN_NAME = result.Data.CAMPAIGN_NAME.replace(/&quot;/g,"");
                  var ADGROUP_NAME = result.Data.ADGROUP_NAME.replace(/&quot;/g,"");

                  field_td.push(AD_NAME); // 広告名
                  field_td.push(URL_NAME); // リンク先URL
                  field_td.push(CAMPAIGN_NAME); // キャンペーン名
                  field_td.push(ADGROUP_NAME); // 広告グループ名
                  field_td.push(result.Data.AD_TYPE); // 広告タイプ
                  field_td.push(result.Data.IMPS); // インプレッション数
                  field_td.push(result.Data.CLICK); // クリック数
                  field_td.push(result.Data.CLICK_RATE); // クリック率
                  field_td.push(result.Data.AVG_DELIVER_RANK); // 平均掲載順位
                  field_td.push(result.Data.COST); // 合計コスト
                  field_td.push(result.Data.AVG_CPC); // 平均CPC
                  field_td.push(result.Data.CONVERSIONS); // 総コンバージョン数
                  field_td.push(result.Data.CONV_RATE); // 総コンバージョン率
                  field_td.push(result.Data.COST_PER_CONV); // コスト/総コンバージョン数
                  // 配列に入れる
                  casper.echo(field_td);
                  });
                  if (total > page * 200) {
                    page++;
                    this.then(ad_page);
                  }
              });
            };
            this.then(ad_page);
          }
        }
      });
    });
  });
});

casper.run();
