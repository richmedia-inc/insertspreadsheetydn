<?php
    require 'vendor/autoload.php';

    $dotenv = new Dotenv\Dotenv(__DIR__);
    $dotenv->load();

    // /**
    //  * YDNをスプレッドシートに出力する。↓これ
    //  * https://docs.google.com/spreadsheets/d/1BRfZJ7fn0E0py6FUVUkAH9GNRnC6n_InLmHYGD5t34Y/edit#gid=0
    //  */
    // ID
    $LOGIN_ID = getenv('LOGIN_ID');
    // Pass
    $LOGIN_PASS = getenv('LOGIN_PASS');
    /**
     * メイン関数
     */
    // 保存用の日付取得
    $date = new DateTime(date("Y-m-d", strtotime("-1 month")));
    $d = $date->format('d');

    //毎月1日はmonthlyレポートを出力。
    if($d == '01'){
      $report_period = 'monthly';
    } else {
      //1日以外はdailyレポートを出力。
      $report_period = 'daily';
    }

    // casperjsの実行コマンドを作成(dailyレポを取得する or 1日であれば先月のデータを取得)
    $prog = ['casperjs --ssl-protocol=any --ignore-ssl-errors=yes --web-security=no insert_ydn.js --user=' . $LOGIN_ID . ' --pass='. $LOGIN_PASS . ' dureycsm --period=daily'];

    // casperjsの実行する
    exec(implode(' ', $prog), $out, $ret);

    //取得できない or 終了ステータスが0以外(正常でない)の場合は処理を終了する
    if(!$out || $ret != 0){
      return;
    }
        
    /****************************
    ここからスプレッドシートの処理 
    *****************************/

    //casoerコマンド実行で取得したデータをスプレッドシートに出力
    date_default_timezone_set('Asia/Tokyo');
    putenv('GOOGLE_APPLICATION_CREDENTIALS=insertydn-302df08ec0aa.json');

    $client = new Google_Client();
    $client->useApplicationDefaultCredentials();
    $client->addScope(Google_Service_Sheets::SPREADSHEETS);
    $client->setApplicationName('InsertYdnData');
    $service = new Google_Service_Sheets($client);
    $spreadsheetId = getenv('SPREADSHEET_ID');

    //取得したYDNの値を出力に適した形に修正する。
    foreach ($out as $key => $value_2) {
      $outs = explode(",", $value_2);
      $out_array[$key] = $outs;
    }

    $out_length = count($out);
    $target_line = $out_length + 1;
    $range = 'data!A2:N'. $target_line;
    $data = [];
    $data[] = new Google_Service_Sheets_ValueRange([
      'range' => $range,
      'values' => $out_array
    ]);
    // Additional ranges to update ...
    $body = new Google_Service_Sheets_BatchUpdateValuesRequest([
      'valueInputOption' => 'USER_ENTERED',
      'data' => $data
    ]);
    $result = $service->spreadsheets_values->batchUpdate($spreadsheetId, $body);
    return true;
